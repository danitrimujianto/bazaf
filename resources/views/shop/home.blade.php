@extends('shop.layouts.main')

@section('title', $title ?? 'Home - ' )
@section('meta_description', $meta_description ?? 'Printyuk Description' )
@section('meta_og_title', $meta_og_title ?? 'Printyuk Title' )
@section('meta_og_description', $meta_og_description ?? 'Printyuk Title' )
@section('meta_og_image', $meta_og_image ?? 'Printyuk Title' )

@section('content')
<div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slideshow>
    <ul class="uk-slideshow-items">
        <li>
            <img src="https://getuikit.com/docs/images/photo.jpg" alt="">
        </li>
        <li>
            <img src="https://getuikit.com/docs/images/light.jpg" alt="">
        </li>
        <li>
            <img src="https://getuikit.com/docs/images/dark.jpg" alt="">
        </li>
    </ul>
    <a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
    <a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slideshow-item="next"></a>
</div>
<div class="uk-width-1-3@s af-product-title">
    <span style="color: #b2d022;">Open</span> <span style="color: #0aa722;">New PO</span> 
</div>
<div class="uk-section uk-padding-remove">
    <div class="af-product">
        <div class="uk-child-width-1-4@m" uk-grid>
            @for($i=1;$i<=4;$i++)
            <div>
                <div class="uk-card uk-card-default">
                    <div class="uk-card-media-top">
                        <img src="https://getuikit.com/docs/images/photo.jpg" alt="">
                    </div>
                    <div class="uk-card-body">
                        <h3 class="uk-card-title">Media Top</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
                        <p>
                        <span><strong>Harga:</strong></span><br>
                        <span>Rp 8.000,-</span>
                        </p>
                        <button class="uk-button uk-button-primary uk-width-1-1">Create PO</button>
                    </div>
                </div>
            </div>
            @endfor
        </div>
    </div>
</div>
@endsection