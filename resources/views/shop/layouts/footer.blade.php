<div class="uk-section uk-background-primary uk-padding-remove">
    <div class="af-footer">
        <div class="uk-child-width-1-3@s" uk-grid>
            <div>
                <div class="uk-grid-small uk-flex-middle" uk-grid>
                    <div class="uk-width-auto">
                        <span uk-icon="icon: receiver; ratio: 2"></span>
                    </div>
                    <div class="uk-width-expand">
                        <h3 class="uk-card-title uk-margin-remove-bottom">MEMESAN</h3>
                        <p class="uk-text-meta uk-margin-remove-top">Pesan makanan yang ingin Anda di makasakinaja.com</p>
                    </div>
                </div>
            </div>
            <div>
                <div class="uk-grid-small uk-flex-middle" uk-grid>
                    <div class="uk-width-auto">
                        <span uk-icon="icon: credit-card; ratio: 2"></span>
                    </div>
                    <div class="uk-width-expand">
                        <h3 class="uk-card-title uk-margin-remove-bottom">PEMBAYARAN</h3>
                        <p class="uk-text-meta uk-margin-remove-top">Lakukan pembayaran melalui transfer bank</p>
                    </div>
                </div>
            </div>
            <div>
                <div class="uk-grid-small uk-flex-middle" uk-grid>
                    <div class="uk-width-auto">
                        <span uk-icon="icon: cart; ratio: 2"></span>
                    </div>
                    <div class="uk-width-expand">
                        <h3 class="uk-card-title uk-margin-remove-bottom">KIRIM</h3>
                        <p class="uk-text-meta uk-margin-remove-top">Kami akan mengirim langsung kerumah Anda</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>