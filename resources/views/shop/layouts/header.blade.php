<div class="uk-section uk-padding-remove">
    <div class="af-header">
        <nav class="uk-navbar-container uk-background-default" uk-navbar>
            <div class="uk-navbar-left">
                <a class="uk-navbar-item uk-logo" href="#"><img src="{{ asset('common/img/logo-hr.png') }}" style="height: 40px;" /></a>
            </div>
            <div class="uk-navbar-right">
                <a class="uk-hidden@m uk-navbar-toggle" uk-navbar-toggle-icon href="#"></a>
                <ul class="uk-visible@m uk-navbar-nav">
                    <li><a href="#"><span uk-icon="user"></span>&nbsp;&nbsp;&nbsp;Masuk</a></li>
                    <li class="active"><a href="#">Daftar</a></li>
                </ul>
            </div>
        </nav>
    </div>
</div>