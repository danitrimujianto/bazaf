<!DOCTYPE html>
<html lang="id">
    <head>
        <title>@yield('title'){{ ($separator ?? ' - ' ) . ( $app_name ?? env('APP_NAME') ?? 'alfatih.com' )}}</title>
        @yield('meta')
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="@yield('meta_description')">
        <meta name="keywords" content="@yield('meta_keywords')">
        <meta name="og:title" property="og:title" content="@yield('meta_og_title')">
        <meta name="og:description" property="og:description" content="@yield('meta_og_description')">
        <meta name="og:image" property="og:image" content="@yield('meta_og_image')">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Styles -->
        {{-- <link href="https://fonts.googleapis.com/css?family=McLaren&display=swap" rel="stylesheet"> --}}
        <link rel="stylesheet" href="{{ asset( 'shop/css/font.css' ) }}" />
        <link rel="stylesheet" href="{{ asset( 'shop/css/uikit.min.css' ) }}" />
        <link rel="stylesheet" href="{{ asset( 'shop/css/common.css' ) }}" />
        <link href="{{ asset( 'shop/css/all.min.css' ) }}" rel="stylesheet">
        <script src="{{ asset( 'shop/js/jquery-3.3.1.min.js' )}}"></script>
        <script src="{{ asset( 'shop/js/uikit.min.js' ) }}"></script>
        <script src="{{ asset( 'shop/js/uikit-icons.min.js' )}}"></script>
        @stack('style')
        @stack('script_top')
    </head>
    <body>
        @include('shop.layouts.header')
        @yield('content')
        @include('shop.layouts.footer')
        <!-- Script -->
        <script src="{{ asset( 'shop/js/common.js' )}}"></script>
        <script src="{{ asset( 'shop/js/all.min.js' )}}"></script>
        @stack('script_bottom')
    </body>

</html>
