<?php
return [
    'home' => [
        'meta_title' => 'Home',
        'meta_description' => "Salah satu unit usaha hasil wakaf keluarga besar Al Fatih yang hasilnya dikembalikan untuk mendukung pendidikan.",
        "meta_og_title" => 'Home',
        "meta_og_description" => "Salah satu unit usaha hasil wakaf keluarga besar Al Fatih yang hasilnya dikembalikan untuk mendukung pendidikan.",
        "meta_og_image" => asset('common/img/logo-og.png'),
        
    ]
];