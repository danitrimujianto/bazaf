<?php
return [
    'home' => [
        'meta_title' => 'Bazar Al Fatih',
        'meta_description' => "Salah satu unit usaha hasil wakaf keluarga besar Al Fatih yang hasilnya dikembalikan untuk mendukung pendidikan.",
        "meta_og_title" => 'Bazar Al Fatih',
        "meta_og_description" => "Salah satu unit usaha hasil wakaf keluarga besar Al Fatih yang hasilnya dikembalikan untuk mendukung pendidikan.",
        "meta_og_image" => asset('common/img/logo-og.png'),
        
    ]
];