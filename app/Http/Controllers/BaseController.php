<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;

class BaseController extends Controller
{
    public $services_token = FALSE;

    public function __construct()
    {
        // $service = app()->make('Service');
    }
}