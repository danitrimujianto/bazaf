<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\BaseController as AppBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App;
use Auth;

class BaseController extends AppBaseController
{
    protected $settings;
    protected $metta_show;
    protected $user;
    protected $categories;
    protected $product;
    protected $main_data;
    protected $customer;

    public function __construct()
    {
        parent::__construct();
        
    }
}
