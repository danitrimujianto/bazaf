<?php

namespace App\Http\Controllers\Shop;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends BaseController
{
    protected $page_name = 'home';

    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $data = [
            "title" => __("metta.$this->page_name.meta_title"),
            "meta_description" => __("metta.$this->page_name.meta_description"),
            "meta_og_title" => __("metta.$this->page_name.meta_og_title"),
            "meta_og_description" => __("metta.$this->page_name.meta_og_description"),
            "meta_og_image" => __("metta.$this->page_name.meta_og_image")
        ];

        return view('shop.'.$this->page_name, $data);
    }
}