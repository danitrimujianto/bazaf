$(document).ready(function(){

    axios.defaults.headers.common = {
        'X-Requested-With': 'XMLHttpRequest',
        'X-CSRF-TOKEN' : document.querySelector('meta[name="csrf-token"]').getAttribute('content')
    };
    axios.interceptors.response.use(function (response) {
        if(response.data.error){
            UIkit.notification( response.data.error , 'warning');
        }
        return response;
    }, function (error) {

        switch(error.response.status) {
            case 401:
                $('#link-login')[0].click();
                break;
            case 404:
                console.log(error.response);
                break;
            case 412:
                console.log(error.response);
                break;
            default:
                if(error.response.data.error)
                    var errorMsg = error.response.data.error;
                else
                    var errorMsg = 'Oops... Please try again later';
                
                UIkit.notification( errorMsg , 'warning');

          }

        return Promise.reject(error);
    });

    $("#iconPwd").click(function(){
        var target = $(this).parent("div");
        if(target.find("input").attr('type') == 'password')
        {    
            target.find("a").html('<i class="fas fa-eye">'); 
            target.find("input").attr('type', 'text'); 
        }
        else
        {    
            target.find("a").html('<i class="fas fa-eye-slash">'); 
            target.find("input").attr('type', 'password'); 
        }
    });

    $(".iconPwd").click(function(){
        var target = $(this).parent("div");
        if(target.find("input").attr('type') == 'password')
        {    
            target.find("a").html('<i class="fas fa-eye">'); 
            target.find("input").attr('type', 'text'); 
        }
        else
        {    
            target.find("a").html('<i class="fas fa-eye-slash">'); 
            target.find("input").attr('type', 'password'); 
        }
    });

});

function currencyFormat(num, dec) {
    var num = num || 0;
    var dec = dec || 0;

    return (
      'Rp. ' + num
        .toFixed(dec) // always two decimal digits
        .replace('.', ',') // replace decimal point character with ,
        .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    ) // use . as a separator
}

function getProductInfo(api, param){
    return axios.get( api + "?" + param )
    .then(function (response) {
        return response.data;
        
    })
    .catch(function (error) {
        return error.response.data;

    });
}

function loading(action = 'before', elemenId = '#none', html = '____', data = null, disabled = 0, notification = 1){
    
    try{
        if(action == 'before'){
            if(disabled==1)
                $(elemenId).attr('disabled','disabled');
            $(elemenId).html('<span uk-spinner></span>');
        }else{
            setTimeout(function(){

                try{
                    if(data.success){    
                        var message = data.success || 'Success';
                        var status = 'success';
                    }else{
                        var message = data.error || 'Oops... Server Problem, please try again later';
                        var status = 'danger';
                    }
                }catch(e){
                    var message = 'Oops... Server Problem, please try again later';
                    var status = 'danger';
                }
    
                $(elemenId).removeAttr('disabled');
                $(elemenId).html(html);
    
                var option = {
                    message,
                    status,
                    timeout : 5000,
                    pos : 'top-center'
                };
    
                if(notification==1)
                    UIkit.notification( option );
    
            }, 1500);
            
        }
    }catch(e){
        console.log(e);
        var message = data.error || 'Oops... Server Problem, please try again later';
        var status = 'danger';
        
        var option = {
            message,
            status,
            timeout : 5000,
            pos : 'top-center'
        };

        $(elemenId).removeAttr('disabled');
        $(elemenId).html(html);
        
        UIkit.notification( option );
        

    }
}

function setOrPush(target, val) {

    var result = val;

    if (target) {
        result = [target];
        result.push(val);
    }
    return result;
}

function getFormResults(formElement) {
    
    try{

        var formElements = formElement.elements;
        var formParams = {};
        var i = 0;
        var elem = null;
        
        for (i = 0; i < formElements.length; i += 1) {
            elem = formElements[i];
            switch (elem.type) {
                case 'submit':
                    break;
                case 'radio':
                    if (elem.checked) {
                        formParams[elem.name] = elem.value;
                    }
                    break;
                case 'checkbox':
                    if (elem.checked) 
                        formParams[elem.name] = setOrPush(formParams[elem.name], 1);
                    else
                        formParams[elem.name] = setOrPush(formParams[elem.name], 0);
                    break;
                case 'checkbox':
                    if (elem.checked) 
                        formParams[elem.name] = setOrPush(formParams[elem.name], 1);
                    else
                        formParams[elem.name] = setOrPush(formParams[elem.name], 0);
                    break;
                default:
                    formParams[elem.name] = setOrPush(formParams[elem.name], elem.value);
            }
        }
        return formParams;

    }catch(e){
        console.log(e);
        return false;
    }
}

async function pilihKab(id_prov, el, id_choose )
{
    var id_choose = id_choose || null

  var $el = el;
	$el.empty(); // remove old options
	$el.append($("<option></option>").attr("value", ""));//.text("-- Pilih Kabupaten --"));

    // alert("id");
    await axios.get('/addresslist/select_kota/?id_prov='+id_prov)
    .then(function (response) {
        // handle success
        console.log(id_prov,id_choose);
        $.map(response.data, function (item) {

            if(item.type == 'Kabupaten')
                var cityName = 'Kab. ' + item.city_name;
            else
                var cityName = item.city_name;
                
            if(id_choose!==null && id_choose == item.city_id )
                $el.append($('<option data-id="' + item.city_id + '"></option>').attr("value", cityName).text(cityName).attr("selected", 'true'));
            else
                $el.append($('<option data-id="' + item.city_id + '"></option>').attr("value", cityName).text(cityName));
        });

        return true;

    })
    .catch(function (error) {
        // handle error
        console.log(error);

        return false;

    });
}



async function  pilihKec(id, el, id_choose )
{
  var id_choose = id_choose || null

  var $el = el;
	$el.empty(); // remove old options
	$el.append($("<option></option>").attr("value", "")); //.text("-- Pilih Kabupaten --"));

    // alert("id");
    await axios.get('/addresslist/select_district/?id_kab='+id)
    .then(function (response) {
        // handle success
        // console.log(response)
        $.map(response.data, function (item) {
            
            if(id_choose!==null && id_choose == item.subdistrict_id )
                $el.append($('<option data-id="' + item.city_id + '"></option>').attr("value", item.subdistrict_name).text(item.subdistrict_name).attr("selected", 'true'));
            else
                $el.append($('<option data-id="' + item.city_id + '"></option>').attr("value", item.subdistrict_name).text(item.subdistrict_name));
        });

        return true;
    })
    .catch(function (error) {
        // handle error
        console.log(error);
        return false;

    });
}

function countDown(dateString, countDiv, resendDiv){
    // Set the date we're counting down to
    var countDownDate = new Date(dateString).getTime();

    // Update the count down every 1 second
    var now = new Date().getTime();

    var distance =  now - countDownDate ;

    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor( 60 - ( (distance % (1000 * 60)) / 1000) );

    if(days == 0 && hours == 0 && minutes == 0 ){
        var x = setInterval(function() {

         var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));

        // Display the result in the element with id="demo"
        document.getElementById(countDiv).innerHTML = '(' + minutes + ':' + seconds + ')';

        // If the count down is finished, write some text
        if ( minutes < 1 & seconds < 1) {
            clearInterval(x);
            document.getElementById(resendDiv).setAttribute("class", "uk-link");
            document.getElementById(countDiv).innerHTML = "";
        }

        seconds = seconds - 1;

        }, 1000);
    }else{
        document.getElementById(resendDiv).setAttribute("class", "uk-link");
        document.getElementById(countDiv).innerHTML = "";
    }
        
}

function renderMap(position,option) {
    const elementMap = option.elementMap || document.getElementById('map');
    const elementSearch = option.elementSearch || document.getElementById('input-search');
    const elementCoordinate = option.elementCoordinate.value.split(",") || document.getElementById('input-coordinate');
    const elementInfoWindow = option.elementInfoWindow || document.getElementById('info-window');
    
    console.log(elementCoordinate,Number(elementCoordinate[0]),Number(elementCoordinate[1]) );
    var latitude = Number(elementCoordinate[0]) || -6.234055376784218 ;
    var longitude = Number(elementCoordinate[1]) || 106.74549458076477;
    var accuracy = position.coords.accuracy || 1000;
    var coords = new google.maps.LatLng(latitude, longitude);
    
    var mapOptions = {
        zoom: 17,
        center: coords,
        mapTypeControl: false
    };

    
    var map = new google.maps.Map( elementMap,mapOptions);
    
    google.maps.event.addListener(map, 'tilesloaded', function(){
        elementMap.style.position = 'absolute';
        elementMap.style.top = '10px';
        elementMap.style.left = 0;
        elementMap.style.right = 0;
        elementMap.style.bottom = 0;
        elementMap.style.background = 'none';
    });
    
    var options = {
    // types: ['(cities)'],
    componentRestrictions: {country: 'id'}
    };

    var autocomplete = new google.maps.places.Autocomplete(elementSearch, options);
    google.maps.event.trigger(autocomplete, 'place_changed');

    /*  
       var displaySuggestions = function(predictions, status) {
        if (status != google.maps.places.PlacesServiceStatus.OK) {
          alert(status);
          return;
        }
    
        predictions.forEach(function(prediction) {
            var li = document.createElement('li');
            li.appendChild(document.createTextNode(prediction.description));
            document.getElementById('results').appendChild(li);
            });
        };
    
        var service = new google.maps.places.AutocompleteService();
        service.getQueryPredictions({ input: 'printyuk' }, displaySuggestions);
    */

    var infowindow = new google.maps.InfoWindow();
    infowindow.setContent(elementInfoWindow);
    
    var marker = new google.maps.Marker({
    map: map,
    draggable: true,
    animation: google.maps.Animation.DROP,
    position: coords,
    anchorPoint: new google.maps.Point(0, -29)
    });

    var geocoder = new google.maps.Geocoder;
    
    marker.addListener('click', () => infowindow.open(map, marker))
    
    google.maps.event.addListener(marker, 'dragend',
    function(marker) {
        var latLng = marker.latLng;
        lat = Number(latLng.lat());
        lan = Number(latLng.lng());
        latLan = new google.maps.LatLng(lat, lan);
        console.log(marker,latLan);
        geocoder.geocode({'location': latLan}, function(results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
            if (results[1]) {
            console.log(results);
            var request = {
            placeId: results[1].place_id,
            fields: ['name', 'formatted_address', 'place_id', 'geometry', 'icon','address_components']
            };
            var service = new google.maps.places.PlacesService(map);
            service.getDetails(request, function(place, status) {
            if (status === google.maps.places.PlacesServiceStatus.OK) {
                infowindow.close();
                setInfoWindow(place);

            }
            });
            } else {
            window.alert('No results found');
            }
        } else {
            window.alert('Geocoder failed due to: ' + status);
        }
        });
        

        
    }
    );

    autocomplete.addListener('place_changed', function() {
    infowindow.close();
    setInfoWindow(autocomplete.getPlace());
    });

    function setInfoWindow(place){

        var place = place || autocomplete.getPlace();
        
        console.log(place);
        if (!place.geometry) {
        // User entered the name of a Place that was not suggested and
        // pressed the Enter key, or the Place Details request failed.
        window.alert("No details available for input: '" + place.name + "'");
        return;
        }

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
        map.fitBounds(place.geometry.viewport);
        } else {
        map.setCenter(place.geometry.location);
        map.setZoom(17);  // Why 17? Because it looks good.
        }
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);

        var address = '';
        if (place.address_components) {
        address = [
            (place.address_components[0] && place.address_components[0].short_name || ''),
            (place.address_components[1] && place.address_components[1].short_name || ''),
            (place.address_components[2] && place.address_components[2].short_name || '')
        ].join(' ');
        }

        elementInfoWindow.children['place-icon'].src = place.icon;
        elementInfoWindow.children['place-name'].textContent = place.name;
        elementInfoWindow.children['place-address'].textContent = address;
        
        elementCoordinate.value = marker.getPosition().lat() + ',' + marker.getPosition().lng();

        infowindow.open(map, marker);
    }

}

function cartAdd(api, param){
    
    return axios.post( api + "?" + param,{
       
        cart : localStorage.getItem('cart')
        ,
        headers: {
            withCredentials: true,
          }
    } )
    .then(function (response) {

        if(response.data.success){
            console.log( response.data.data);
            $('#dropdown-cart').html(response.data.data.html);
            localStorage.setItem('cart', JSON.stringify(response.data.data), 365 );
        }

        return response.data;
        
    })
    .catch(function (error) {
        console.log(error);
        return error.response.data;

    });

}

async function modalConfirm(detail){
     return UIkit.modal.confirm(detail.message || 'Are you sure ?' ,{
         style : {
             width : "350px"
         }
     }).then( function () {
        return true;
    }, function () {
        return false
    });
}

async function cartRefresh(){
    return await axios.post( '/cart/refresh' ,{
        cart : localStorage.getItem('cart'),
        headers: {
            withCredentials: true,
          }
    })
    .then(function (response) {

        let badge = '';

        if(response.data.success){

            if(response.data.data.total > 0)
                 badge = '<span class="uk-badge">'+response.data.data.total+'</span>';
            
            $('#btn-cart').html('<img src="/common/img/icons-8-shopping-cart-disabled.png" class="icon-common" id="icon-cart">' + badge);
            $('#dropdown-cart').html(response.data.data.html);
            localStorage.setItem('cart', JSON.stringify(response.data.data), 365 );
            // console.log(response.data);
        }

        return response.data;
        
    })
    .catch(function (error) {
        console.log(error);
        return error.response.data;

    });
}


async function calculateCheckout(e, address_id, data_shipping_code, data_shipping_service_code ){

    // e.preventDefault();
    // e.stopPropagation();
    // e.blur;
    // return false;
    let element_sub_total = document.getElementById('sub-total');
    let element_shipping_cost = document.getElementById('shipping-cost');
    let element_total = document.getElementById('total-price');
    // var element_discount = document.getElementById('discount');
    
    element_sub_total.innerHTML = '<span uk-spinner></span>';
    element_shipping_cost.innerHTML = '<span uk-spinner></span>';
    element_total.innerHTML = '<span uk-spinner></span>';

    address_id = address_id || '0';
    let shipping_courier_code = data_shipping_code || '0';
    let shipping_courier_service_code = data_shipping_service_code || '0';

    await axios.post( '/cart/calculate-checkout',{
            address_id,
            shipping_courier_code,
            shipping_courier_service_code,

        })
        .then(function (response) {

            if(response.data.success){
                element_sub_total.innerHTML = '<span>'+ response.data.data.sub_total_text +'</span>';
                element_shipping_cost.innerHTML = '<span>'+ response.data.data.shipping_cost_text +'</span>';
                element_total.innerHTML = '<span>'+ response.data.data.total_text +'</span>';
                button_buy.style.visibility = "visible";
                
                console.log( 'Checkout Calculated');
                return response.data;
                
            }else{
                element_sub_total.innerHTML = '<span>-</span>';
                element_shipping_cost.innerHTML = '<span>-</span>';
                element_total.innerHTML = '<span>-</span>';
            }

            return response.data;
        })
        .catch(function (error) {
            console.log(error);
            element_sub_total.innerHTML = '<span>-</span>';
            element_shipping_cost.innerHTML = '<span>-</span>';
            element_total.innerHTML = '<span>-</span>';

            return error.response;
        }
    ); 

}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
}
  
function checkCookie(name) {
    var name = getCookie(name);
    // console.log (name);
    if (name == "") 
      return false;
    else
     return true;
} 

var py = {
    api : {
        get : async function(url, param){
            return await axios.get( url + param)
            .then(function (response) {
                return response.data;
            })
            .catch(function (error) {
                return error.response;
            });
        },
        post : async function(url, param, data){
            return await axios.post( url + param, data )
            .then(function (response) {
                return response.data;
            })
            .catch(function (error) {
                return error.response;
            });
        },
        patch : async function(url, param, data){
            return await axios.patch( url + param, {data} )
            .then(function (response) {
                return response.data;
            })
            .catch(function (error) {
                return error.response;
            });
        },
        delete : async function(url, param){
            return await axios.delete( url + param)
            .then(function (response) {
                return response.data;
            })
            .catch(function (error) {
                return error.response;
            });
        }, 
    },
    widget : {
        modal : {
            address_list :  async function(el, callback){
                var data = await py.api.get('/customer/modal/address-list', '')
                // console.log(data);
                if(data){
                    el.innerHTML = data;
                    UIkit.modal(el).show();
                }else
                    console.log(data);
            }
        }
    }
}

function initApp(auth){
    
    auth = auth || false;

    if(auth)
        cartRefresh();

    $('.search-box').autocomplete({
        paramName: 'q',
        serviceUrl: '/',
        onSelect: function (suggestion) {
            console.log(suggestion, suggestion.slug.title )
            window.location.href = "/" + suggestion.slug.title;
            // alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
        }
    });
}