function gmaps(option){

    const elementMap = option.elementMap || document.getElementById('map');
    const errorMsgUnsupported = option.errorMsgUnsupported || 'Your browser not support for GPS Future';
    const errorMsgGeoDisabled = option.errorMsgGeoDisabled || 'Please Enable your GPS Future';
    const elementSearch = option.elementSearch || document.getElementById('input-search');
    const elementCoordinate = option.elementCoordinate || document.getElementById('input-coordinate');
    const elementInfoWindow = option.elementInfoWindow || document.getElementById('info-window');

    var firstScript = document.getElementsByTagName('script')[0],
    js = document.createElement('script');
    js.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBKXJSyi8yiECep2fzFNjbQCwDRqmUzKeU&libraries=places';
    js.onload = function () {
    
        function initMap(){
            
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(renderMap,errorMap,{maximumAge:10000, timeout:5000, enableHighAccuracy: true});
            } else {
                alert( errorMsgUnsupported );
            }
        }
        
        function errorMap(msg) {
            // alert(errorMsgGeoDisabled);
            var position = {
                coords : {
                    latitude : -6.234055376784218,
                    longitude : 106.74549458076477,
                    accuracy : 1000
                }
            };

            renderMap(position);
        }
      
        function renderMap(position,option) {
            console.log(position);
        
            var latitude = position.coords.latitude || -6.234055376784218 ;
            var longitude = position.coords.longitude || 106.74549458076477;
            var accuracy = position.coords.accuracy || 1000;
            var coords = new google.maps.LatLng(latitude, longitude);
            
            var mapOptions = {
                zoom: 17,
                center: coords,
                mapTypeControl: false
            };

            
            var map = new google.maps.Map( elementMap,mapOptions);
            
            google.maps.event.addListener(map, 'tilesloaded', function(){
                elementMap.style.position = 'absolute';
                elementMap.style.top = '10px';
                elementMap.style.left = 0;
                elementMap.style.right = 0;
                elementMap.style.bottom = 0;
                elementMap.style.background = 'none';
            });
            
            var options = {
            // types: ['(cities)'],
            componentRestrictions: {country: 'id'}
            };
        
            var autocomplete = new google.maps.places.Autocomplete(elementSearch, options);
            google.maps.event.trigger(autocomplete, 'place_changed');
    
            /*  
               var displaySuggestions = function(predictions, status) {
                if (status != google.maps.places.PlacesServiceStatus.OK) {
                  alert(status);
                  return;
                }
            
                predictions.forEach(function(prediction) {
                    var li = document.createElement('li');
                    li.appendChild(document.createTextNode(prediction.description));
                    document.getElementById('results').appendChild(li);
                    });
                };
            
                var service = new google.maps.places.AutocompleteService();
                service.getQueryPredictions({ input: 'printyuk' }, displaySuggestions);
            */

            var infowindow = new google.maps.InfoWindow();
            infowindow.setContent(elementInfoWindow);
            
            var marker = new google.maps.Marker({
            map: map,
            draggable: true,
            animation: google.maps.Animation.DROP,
            position: coords,
            anchorPoint: new google.maps.Point(0, -29)
            });
        
            var geocoder = new google.maps.Geocoder;
            
            marker.addListener('click', () => infowindow.open(map, marker))
            
            google.maps.event.addListener(marker, 'dragend',
            function(marker) {
                var latLng = marker.latLng;
                lat = Number(latLng.lat());
                lan = Number(latLng.lng());
                latLan = new google.maps.LatLng(lat, lan);
                console.log(marker,latLan);
                geocoder.geocode({'location': latLan}, function(results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
                    console.log(results);
                    var request = {
                    placeId: results[1].place_id,
                    fields: ['name', 'formatted_address', 'place_id', 'geometry', 'icon','address_components']
                    };
                    var service = new google.maps.places.PlacesService(map);
                    service.getDetails(request, function(place, status) {
                    if (status === google.maps.places.PlacesServiceStatus.OK) {
                        infowindow.close();
                        setInfoWindow(place);
        
                    }
                    });
                    } else {
                    window.alert('No results found');
                    }
                } else {
                    window.alert('Geocoder failed due to: ' + status);
                }
                });
                
        
                
            }
            );
        
            autocomplete.addListener('place_changed', function() {
            infowindow.close();
            setInfoWindow(autocomplete.getPlace());
            });
        
            function setInfoWindow(place){
        
                var place = place || autocomplete.getPlace();
                
                console.log(place);
                if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
                }
        
                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
                } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);  // Why 17? Because it looks good.
                }
                marker.setPosition(place.geometry.location);
                marker.setVisible(true);
        
                var address = '';
                if (place.address_components) {
                address = [
                    (place.address_components[0] && place.address_components[0].short_name || ''),
                    (place.address_components[1] && place.address_components[1].short_name || ''),
                    (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
                }
        
                elementInfoWindow.children['place-icon'].src = place.icon;
                elementInfoWindow.children['place-name'].textContent = place.name;
                elementInfoWindow.children['place-address'].textContent = address;
                
                elementCoordinate.value = marker.getPosition().lat() + ',' + marker.getPosition().lng();

                infowindow.open(map, marker);
            }
        
        }

        initMap(elementMap, elementSearch, elementCoordinate, elementInfoWindow );
    };
    firstScript.parentNode.insertBefore(js, firstScript);
}